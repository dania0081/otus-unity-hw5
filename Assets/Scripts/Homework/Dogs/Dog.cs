using Homework.Common;
using Homework.Movement;
using System;
using UnityEngine;

namespace Homework.Dogs
{
    /**
     * TODO:
     * 1. Добавить всем собакам способность гавкать: достаточно метода, пишущего в Unity-консоль строку с сообщение. - Сделано
     * 2. HappyDog должен гавкать более радостно. - Сделано
     * 3. (сложно) Пусть собаки гавкают только тогда, когда меняют направление движения. - Сделано
     */
    public abstract class Dog : MonoBehaviour, IColorChangeable
    {
        public abstract void ChangeColor();
        public abstract void Voice();

        protected Move Move;

        public event Action OnVoice;

        protected void Start()
        {
            OnVoice += Voice;
            if (transform.GetComponent<SadDog>() != null)
            {
                Move = new Walk(this, -4, 4, 1, OnVoice);
            }
            else
            {
                Move = new Run(this, -4, 4, 1, OnVoice);
            }
            InputController.Instance.OnColorChanged += OnColorChanged;
        }

        protected void Update()
        {
            Move.Execute();
        }

        private void OnDestroy()
        {
            InputController.Instance.OnColorChanged -= OnColorChanged;
        }

        protected SpriteRenderer GetSpriteRenderer(SpriteRenderer _spriteRenderer)
        {
            if (_spriteRenderer == null)
                _spriteRenderer = GetComponentInChildren<SpriteRenderer>();

            return _spriteRenderer;
        }

        private void OnColorChanged()
        {
            ChangeColor();
        }
    }
}