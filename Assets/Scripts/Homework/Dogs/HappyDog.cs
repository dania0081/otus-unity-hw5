using UnityEngine;

namespace Homework.Dogs
{
    public class HappyDog : Dog
    {
        private SpriteRenderer _spriteRenderer;

        public override void ChangeColor()
        {
            var random = new System.Random();
            var red = (float)random.NextDouble();
            GetSpriteRenderer(_spriteRenderer).color = new Color(0.5f + red / 2, 0.1f, 0.1f);
        }

        public override void Voice()
        {
            Debug.Log("���-���!)");
        }
    }
}