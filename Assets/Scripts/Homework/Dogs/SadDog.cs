using UnityEngine;

namespace Homework.Dogs
{
    /**
     * TODO:
     * 1. Реализовать этот тип по аналогии с HappyDog. - Сделано
     * 2. Грустная собака должна перекрашиваться в оттенки синего. - Сделано
     * 3. (сложно) Перенести метод GetSpriteRenderer в более подходящее место. - Сделано
     */
    public class SadDog : Dog
    {

        private SpriteRenderer _spriteRenderer;

        public override void ChangeColor()
        {
            var random = new System.Random();
            var blue = (float)random.NextDouble();
            GetSpriteRenderer(_spriteRenderer).color = new Color(0.1f, Mathf.Clamp(blue / 2f, 0, 3), 1f);
        }

        public override void Voice()
        {
            Debug.Log("Гав(");
        }
    }
}