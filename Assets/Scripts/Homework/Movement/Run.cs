using System;
using UnityEngine;

namespace Homework.Movement
{
    /**
     * TODO:
     * 1. Реализовать этот тип перемещения по аналогии с Walk, но отличающийся от него,
     * например, пусть перемещение будет по окружности заданного радиуса. - Сделано (на уроке сказали, можно вертикально)
     * 2. Заменить передвижение у HappyDog и/или SadDog этим типом. Убедиться, что он работает. - Сделано
     */
    public class Run : Move
    {
        private readonly float _minY;
        private readonly float _maxY;
        private readonly float _speed;
        private int _direction = 1;
        private Action OnVoice;

        public Run(MonoBehaviour owner) : base(owner)
        {
        }

        public Run(MonoBehaviour owner, float minY, float maxY, float speed, Action voice) : base(owner)
        {
            _minY = minY;
            _maxY = maxY;
            _speed = speed;
            OnVoice = voice;
        }

        public override void Execute()
        {
            var newPosition = Owner.transform.position;
            newPosition.y += _direction * Time.deltaTime * _speed;

            Owner.transform.position = newPosition;

            if (newPosition.y > _maxY)
            {
                OnVoice.Invoke();
                _direction = -1;
            }
            else if (newPosition.y < _minY)
            {
                OnVoice.Invoke();
                _direction = 1;
            }
        }
    }
}