using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Homework
{
    /**
     * TODO:
     * 1. Найти примеры полиморфизма в уже написанном коде и в том, что будет написан вами. - Использование наследования, интерфейсов, абстрактных классов, переопределение методов - Сделано
     * 2. Реализовать удаление объектов из коллекции _spawnedObjects. - Сделано
     * 3. Заменить тип коллекции на более подходящий к данному случаю. Объяснить, если замена не требуется. - Сделано
     */
    public class Spawner : MonoBehaviour
    {
        [SerializeField]
        private int _totalAmount;

        [SerializeField]
        private float _spawnDelay;

        [SerializeField]
        private float _removeDelay;

        [SerializeField]
        private List<GameObject> _objectsToSpawn;

        private readonly Queue<GameObject> _spawnedObjects = new Queue<GameObject>();


        void Start()
        {
            StartCoroutine(SpawnNext());
        }

        private IEnumerator SpawnNext()
        {
            var random = new System.Random();
            int i;

            while (true)
            {
                yield return new WaitForSeconds(_spawnDelay);

                if (_spawnedObjects.Count < _totalAmount)
                {
                    i = random.Next(_objectsToSpawn.Count);

                    var spawnedObject = Instantiate(_objectsToSpawn[i], transform);

                    _spawnedObjects.Enqueue(spawnedObject);
                }

                yield return new WaitForSeconds(_removeDelay);

                if (_spawnedObjects.Count >= _totalAmount)
                {
                    Destroy(_spawnedObjects.Dequeue());
                    Debug.Log(_spawnedObjects.Count);
                }
            }
        }
    }
}